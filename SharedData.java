 class SharedData {
	int data;
	public static void main(String args[]) throws Exception {
		SharedData sd = new SharedData();
		new Procedur(sd);
		new Consumer(sd);
	}
	synchronized void set(int value) {
		System.out.println("Generate " +value);
		data = value;
	}
	
	synchronized int get() {
		System.out.println("Get " +data);
		return data;
	}
}

class Procedur implements Runnable {
	SharedData sd;
	
	public Procedur(SharedData sd) {
		// TODO Auto-generated constructor stub
		this.sd = sd;
		new Thread(this, "Producer").start();
	}
	
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0 ; i<10; i++) {
			sd.set((int)(Math.random()*100));
		}
	}
}

class Consumer implements Runnable {
	SharedData sd;
	
	Consumer (SharedData sd){
		this.sd = sd;
		new Thread(this, "Consumer").start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i=0; i<10; i++)
			sd.get();
	}
	
}


class TestProducerCunsomer {
	
}
